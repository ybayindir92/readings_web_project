# Readings

Readings is a web project which contains lovely quotes. 
A user needs to be signed up for discovering the application.
Quotes can be added to user's favorite list. 

Readings has a expanding database.
New quotes comes from [QuoteCollector](https://bitbucket.org/ybayindir92/quotecollector) application which is reachable on this repository.
Integration is provided via rabbit messaging broker.

Currently there are two profiles.
* Standalone: The application can work without any external system. Quotes and user information is stored in memory database.
* Local: Requires local mysql database, rabbit messaging broker and QuoteCollector application. Configuration details can be found in application properties.

The application is highly covered with unit and integration tests. Even if it is open for improvement, new features won't be applied for now.

##### Technologies
* Spring Security
* Spring Mvc
* Spring Amqp
* Spring Data Jpa
* Liquibase
* Mysql
* H2
* Gradle

