package com.yasemin.readings.config;

import com.yasemin.readings.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.HashSet;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SecurityConfigTest {

    private static final String VALID_USERNAME = "user";
    private static final String VALID_PASSWORD = "1";
    private static final String LOGOUT_URL = "/leave";
    private static final String LOGOUT_REDIRECT_URL = "/login?logout";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @MockBean
    private UserService userService;

    @Before
    public void setUp() {
        String encodedPassword = passwordEncoder.encode(VALID_PASSWORD);
        UserDetails userDetails =
                new org.springframework.security.core.userdetails.User(VALID_USERNAME, encodedPassword, new HashSet<>());
        given(userService.loadUserByUsername(VALID_USERNAME)).willReturn(userDetails);
    }

    @Test
    public void contentShouldBeProtected() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/"))
                .andExpect(status().is3xxRedirection())
                .andReturn();

        assertThat(mvcResult.getResponse().getRedirectedUrl()).endsWith("/login");
    }

    @Test
    public void resourcesShouldBeAccessible() throws Exception {
        mockMvc.perform(get("/webjar/some")).andExpect(unauthenticated());
        mockMvc.perform(get("/css/some")).andExpect(unauthenticated());
        mockMvc.perform(get("/js/some")).andExpect(unauthenticated());
    }

    @Test
    public void shouldLoginWithCorrectCredentials() throws Exception {
        mockMvc.perform(formLogin().user(VALID_USERNAME).password(VALID_PASSWORD))
                .andExpect(authenticated());
    }

    @Test
    public void shouldNotLoginWithWrongCredentials() throws Exception {
        mockMvc.perform(formLogin().user("invalid").password("invalid"))
                .andExpect(unauthenticated());
    }

    @Test
    public void shouldLogout() throws Exception {
        MvcResult mvcResult = mockMvc.perform(formLogin().user(VALID_USERNAME).password(VALID_PASSWORD))
                .andExpect(authenticated()).andReturn();

        MockHttpSession httpSession = (MockHttpSession) mvcResult.getRequest().getSession(false);

        mockMvc.perform(post(LOGOUT_URL).with(csrf()).session(httpSession))
                .andExpect(redirectedUrl(LOGOUT_REDIRECT_URL));
    }

}