package com.yasemin.readings.service;

import com.yasemin.readings.model.User;
import com.yasemin.readings.repository.RoleRepository;
import com.yasemin.readings.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Collections;

import static java.util.Collections.emptySet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplUnitTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Mock
    private RoleRepository roleRepository;

    @InjectMocks
    private UserServiceImpl userService;

    @Test
    public void shouldSaveUser() {
        // given
        final User user = new User();
        final String encodedPassword = "encodedPassword";
        given(bCryptPasswordEncoder.encode(any())).willReturn(encodedPassword);
        given(roleRepository.findAll()).willReturn(Collections.emptyList());

        // when
        userService.save(user);

        // then
        assertEquals(user.getPassword(), encodedPassword);
        assertTrue(user.getRoles().isEmpty());
        verify(userRepository).save(user);
    }

    @Test(expected = UsernameNotFoundException.class)
    public void shouldThrowExceptionIfUserNotFound() {
        // given
        final String username = "username";
        given(userRepository.findByUsername(username)).willReturn(null);

        // when
        userService.loadUserByUsername(username);
    }

    @Test
    public void shouldLoadUserByUsername() {
        // given
        final String username = "testUser";
        final String password = "testPassword";
        User user = User.builder().username(username).roles(emptySet()).password(password).build();
        given(userRepository.findByUsername(username)).willReturn(user);

        // when
        final UserDetails userDetails = userService.loadUserByUsername(username);

        // then
        assertEquals(userDetails.getUsername(), username);
        assertEquals(userDetails.getPassword(), password);
        assertTrue(userDetails.getAuthorities().isEmpty());
    }
}