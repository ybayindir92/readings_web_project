package com.yasemin.readings.service;

import com.yasemin.readings.messaging.QuoteResource;
import com.yasemin.readings.messaging.QuoteResourceToQuoteConverter;
import com.yasemin.readings.model.Quote;
import com.yasemin.readings.model.UserQuote;
import com.yasemin.readings.repository.QuoteRepository;
import com.yasemin.readings.repository.UserQuoteRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.emptyList;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class QuoteServiceUnitTest {

    @Mock
    private QuoteRepository quoteRepository;

    @Mock
    private UserQuoteRepository userQuoteRepository;

    @Mock
    private QuoteResourceToQuoteConverter converter;

    @InjectMocks
    private QuoteService quoteService;

    @Captor
    private ArgumentCaptor<UserQuote> captor;

    @Test
    public void shouldSaveQuote() {
        // given
        final Quote quote = Quote.builder().textHash("hash").build();
        when(converter.convert(any())).thenReturn(quote);

        // when
        quoteService.save(any());

        // then
        verify(quoteRepository).save(quote);
    }

    @Test
    public void shouldNotSaveQuote() {
        // given
        final Quote quote = Quote.builder().textHash("hash").build();
        given(converter.convert(any())).willReturn(quote);
        given(quoteRepository.findByTextHash(quote.getTextHash())).willReturn(Optional.of(quote));

        // when
        quoteService.save(new QuoteResource());

        // then
        verify(quoteRepository, never()).save(any());
    }

    @Test
    public void shouldReturnRandomQuote() {
        // given
        final List<Quote> quotes = Arrays.asList(new Quote(), new Quote());
        given(quoteRepository.findAll()).willReturn(quotes);

        // when
        final Quote randomQuote = quoteService.getRandomQuote();

        // then
        assertTrue(quotes.contains(randomQuote));
    }

    @Test
    public void shouldReturnEmptyQuote() {
        // given
        given(quoteRepository.findAll()).willReturn(emptyList());

        // when
        final Quote emptyQuote = quoteService.getRandomQuote();

        // then
        assertNotNull(emptyQuote);
    }

    @Test
    public void shouldAddToFavourite() {
        // given
        final String userId = "testUser";
        final long quoteId = 1L;
        final UserQuote userQuote = UserQuote.builder().userId(userId).quoteId(quoteId).build();

        // when
        quoteService.addToFavourites(userId, quoteId);

        // then
        verify(userQuoteRepository).save(captor.capture());
        assertEquals(userQuote, captor.getValue());
    }

    @Test
    public void shouldGetFavouriteQuotes() {
        // given
        final String userId = "testUser";

        // when
        quoteService.getFavourites(userId);

        // then
        verify(quoteRepository).getFavouriteQuotes(userId);
    }

    @Test
    public void shouldRemoveFromFavourite() {
        // given
        final String userId = "testUser";
        final Long quoteId = 1L;
        final UserQuote userQuote = new UserQuote();
        given(userQuoteRepository.findUserQuoteByUserIdAndQuoteId(userId, quoteId)).willReturn(userQuote);

        // when
        quoteService.removeFromFavourites(userId, quoteId);

        // then
        verify(userQuoteRepository).delete(userQuote);
    }

    @Test
    public void shouldFindAll() {
        // when
        quoteService.findAll();

        // then
        verify(quoteRepository).findAll();
    }

    @Test
    public void shouldGetQuotesExceptFavourite() {
        // given
        final String userId = "testUser";

        // when
        quoteService.findAllExcept(userId);

        // then
        verify(quoteRepository).getQuotesExceptFavourites(userId);
    }

}