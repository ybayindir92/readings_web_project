package com.yasemin.readings.messaging;

import com.yasemin.readings.model.Quote;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(JUnit4.class)
public class QuoteResourceToQuoteConverterUnitTest {

    @Test
    public void shouldConvertToQuote() {
        // given
        final QuoteResourceToQuoteConverter converter = new QuoteResourceToQuoteConverter();
        QuoteResource quoteResource = QuoteResource.builder()
                .quote("quote")
                .author("author")
                .cat("category").build();

        // when
        final Quote actual = converter.convert(quoteResource);

        // then
        assertEquals("quote", actual.getText());
        assertEquals("author", actual.getAuthor());
        assertEquals("CATEGORY", actual.getCategory());
        assertNotNull(actual.getTextHash());
    }

}