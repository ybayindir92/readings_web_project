package com.yasemin.readings.messaging;

import com.yasemin.readings.service.QuoteService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class QuoteMessagingConfigurationUnitTest {

    @Mock
    private QuoteService quoteService;

    @InjectMocks
    private QuoteMessagingConfiguration quoteMessagingConfiguration;

    @Test
    public void shouldSaveNewQuote() {
        // when
        quoteMessagingConfiguration.receiveMessage("{}");

        // then
        verify(quoteService).save(any());
    }

}