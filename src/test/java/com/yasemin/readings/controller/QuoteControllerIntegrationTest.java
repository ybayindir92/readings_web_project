package com.yasemin.readings.controller;

import com.yasemin.readings.model.Quote;
import com.yasemin.readings.repository.UserQuoteRepository;
import com.yasemin.readings.service.QuoteService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(username = QuoteControllerIntegrationTest.TEST_USER)
public class QuoteControllerIntegrationTest {

    static final String TEST_USER = "testUser";
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private QuoteService quoteService;

    @MockBean
    private UserQuoteRepository userQuoteRepository;

    @Test
    public void shouldReturnQuoteViewWithQuotes() throws Exception {
        // given
        final List<Quote> quotes = new ArrayList<>();
        given(quoteService.findAll()).willReturn(quotes);

        // when
        final ResultActions resultActions = mockMvc.perform(get("/quotes"));

        // then
        resultActions.andExpect(model().attribute("quotes", quotes));
        resultActions.andExpect(view().name("quotes"));
    }

    @Test
    public void shouldAddFavouriteQuoteAndReturnQuoteView() throws Exception {
        // given
        Quote quote = Quote.builder().id(1L).build();

        // when
        final ResultActions resultActions = mockMvc.perform(post("/addFavourite/{quoteId}", quote.getId()).with(csrf()));

        // then
        verify(quoteService).addToFavourites(TEST_USER, quote.getId());
        resultActions.andExpect(redirectedUrl("/quotes"));
    }

    @Test
    public void shouldReturnFavouriteQuotes() throws Exception {
        // given
        final List<Quote> quotes = new ArrayList<Quote>();
        given(quoteService.getFavourites(anyString())).willReturn(quotes);

        // when
        final ResultActions resultActions = mockMvc.perform(get("/favourites"));

        // then
        resultActions.andExpect(model().attribute("favouriteQuotes", quotes));
        resultActions.andExpect(view().name("favourite"));
    }

    @Test
    public void shouldRemoveFromFavouriteQuotes() throws Exception {
        // given
        final Quote quote = Quote.builder().id(1L).build();

        // when
        final ResultActions resultActions = mockMvc.perform(post("/remove/{quoteId}", quote.getId()).with(csrf()));

        // then
        verify(quoteService).removeFromFavourites(TEST_USER, quote.getId());
        resultActions.andExpect(redirectedUrl("/favourites"));
    }

}