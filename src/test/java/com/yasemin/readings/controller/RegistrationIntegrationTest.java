package com.yasemin.readings.controller;

import com.yasemin.readings.model.User;
import com.yasemin.readings.service.UserService;
import org.apache.logging.log4j.util.Strings;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.thymeleaf.util.StringUtils.randomAlphanumeric;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RegistrationIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Test
    public void registrationPageShouldBeAccessible() throws Exception {
        mockMvc.perform(get("/registration")).andExpect(unauthenticated());
    }

    @Test
    public void shouldRegisterSuccessfully() throws Exception {
        mockMvc
                .perform(
                        post("/registration")
                                .with(csrf())
                                .param("username", randomAlphanumeric(8))
                                .param("firstName", randomAlphanumeric(8))
                                .param("lastName", randomAlphanumeric(8))
                                .param("password", "somePassword")
                                .param("passwordConfirm", "somePassword")
                )
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/login?registered"));
    }

    @Test
    public void shouldNotRegisterWithMissingData() throws Exception {
        mockMvc
                .perform(
                        post("/registration")
                                .with(csrf())
                                .param("username", Strings.EMPTY)
                                .param("password", randomAlphanumeric(8))
                )
                .andExpect(model().attributeHasFieldErrors("user", "username"));
    }

    @Test
    public void shouldNotRegisterWithUnmatchingPasswords() throws Exception {
        mockMvc
                .perform(
                        post("/registration")
                                .with(csrf())
                                .param("password", "111")
                                .param("passwordConfirm", "222")
                )
                .andExpect(model().attributeHasFieldErrorCode("user", "passwordConfirm", "validation.unmatchingPasswords"));
    }

    @Test
    public void shouldNotRegisterWithExistingAccount() throws Exception {

        // given
        given(userService.findByUsername(anyString())).willReturn(new User());

        // when
        mockMvc
                .perform(
                        post("/registration")
                                .with(csrf())
                                .param("username", "username")
                                .param("password", "password")
                )
                .andExpect(model().attributeHasFieldErrorCode("user", "username", "validation.existingAccount"));
    }

}