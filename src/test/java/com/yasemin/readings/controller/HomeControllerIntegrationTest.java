package com.yasemin.readings.controller;

import com.yasemin.readings.model.Quote;
import com.yasemin.readings.service.QuoteService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser
public class HomeControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private QuoteService quoteService;

    @Test
    public void shouldReturnWelcomeViewWithRandomQuote() throws Exception {
        // given
        final Quote quote = new Quote();
        given(quoteService.getRandomQuote()).willReturn(quote);

        // when
        final ResultActions resultActions = mockMvc.perform(get("/welcome"));

        // then
        resultActions.andExpect(model().attribute("quote", quote));
        resultActions.andExpect(view().name("welcome"));
    }

    @Test
    public void shouldReturnLogin() throws Exception {
        // when
        ResultActions resultActions = mockMvc.perform(get("/login"));

        // then
        resultActions.andExpect(view().name("login"));
    }

}
