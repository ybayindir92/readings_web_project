package com.yasemin.readings.messaging;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yasemin.readings.service.QuoteService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.io.IOException;

@Configuration
@ImportAutoConfiguration(RabbitAutoConfiguration.class)
@Profile("!standalone")
@RequiredArgsConstructor
@Slf4j
public class QuoteMessagingConfiguration {

    private static final String QUOTE_QUEUE = "quote-queue";
    private final QuoteService quoteService;

    @Bean
    public SimpleRabbitListenerContainerFactory listenerFactory(ConnectionFactory connectionFactory,
                                                                SimpleRabbitListenerContainerFactoryConfigurer configurer) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        configurer.configure(factory, connectionFactory);
        return factory;
    }

    @RabbitListener(queues = QUOTE_QUEUE)
    public void receiveMessage(final String message) {
        try {
            log.info("Received text: {}", message);
            QuoteResource quoteResource = new ObjectMapper().readValue(message, QuoteResource.class);
            quoteService.save(quoteResource);
        } catch (IOException exception) {
            log.warn("Message processing went wrong: ", exception.getMessage());
        }
    }
}