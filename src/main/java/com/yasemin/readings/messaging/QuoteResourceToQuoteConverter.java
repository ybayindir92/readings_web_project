package com.yasemin.readings.messaging;

import com.yasemin.readings.model.Quote;
import org.springframework.stereotype.Component;

import static com.google.common.hash.Hashing.sha256;
import static java.nio.charset.StandardCharsets.UTF_8;

@Component
public class QuoteResourceToQuoteConverter {

    public Quote convert(final QuoteResource quoteResource) {
        return Quote.builder()
                .author(quoteResource.getAuthor())
                .text(quoteResource.getQuote())
                .textHash(sha256().hashString(quoteResource.getQuote(), UTF_8).toString())
                .category(quoteResource.getCat().toUpperCase()).build();
    }
}
