package com.yasemin.readings.repository;

import com.yasemin.readings.model.UserQuote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserQuoteRepository extends JpaRepository<UserQuote, Long> {

    UserQuote findUserQuoteByUserIdAndQuoteId(String userId, Long quoteId);

}
