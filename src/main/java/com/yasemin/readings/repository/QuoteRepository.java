package com.yasemin.readings.repository;

import com.yasemin.readings.model.Quote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface QuoteRepository extends JpaRepository<Quote, Long> {

    Optional<Quote> findByTextHash(String quoteHash);

    @Query("select q from Quote q where q.id not in (select uq.quoteId from UserQuote uq where uq.userId = ?1)")
    List<Quote> getQuotesExceptFavourites(String userId);

    @Query("select q from Quote q where q.id in (select uq.quoteId from UserQuote uq where uq.userId = ?1)")
    List<Quote> getFavouriteQuotes(String userId);

}
