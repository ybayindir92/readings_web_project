package com.yasemin.readings.service;

import com.yasemin.readings.messaging.QuoteResource;
import com.yasemin.readings.messaging.QuoteResourceToQuoteConverter;
import com.yasemin.readings.model.Quote;
import com.yasemin.readings.model.UserQuote;
import com.yasemin.readings.repository.QuoteRepository;
import com.yasemin.readings.repository.UserQuoteRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class QuoteService {

    private final QuoteRepository quoteRepository;
    private final UserQuoteRepository userQuoteRepository;
    private final QuoteResourceToQuoteConverter converter;

    public void save(QuoteResource quoteResource) {
        Quote quote = converter.convert(quoteResource);
        Optional<Quote> existedQuote = quoteRepository.findByTextHash(quote.getTextHash());
        if (!existedQuote.isPresent()) {
            quoteRepository.save(quote);
        }
    }

    public List<Quote> findAll() {
        return quoteRepository.findAll();
    }

    public List<Quote> findAllExcept(String userId) {
        return quoteRepository.getQuotesExceptFavourites(userId);
    }

    public Quote getRandomQuote() {
        final List<Quote> quotes = quoteRepository.findAll();
        if (quotes.isEmpty()) {
            return new Quote();
        }
        Random random = new Random();
        int n = random.nextInt(quotes.size());
        return quotes.get(n);
    }

    public void addToFavourites(final String userId, final Long quoteId) {
        userQuoteRepository.save(UserQuote.builder().userId(userId).quoteId(quoteId).build());
    }

    public List<Quote> getFavourites(String userId) {
        return quoteRepository.getFavouriteQuotes(userId);
    }

    public void removeFromFavourites(final String userId, final Long quoteId) {
        UserQuote userQuote = userQuoteRepository.findUserQuoteByUserIdAndQuoteId(userId, quoteId);
        userQuoteRepository.delete(userQuote);
    }

}

