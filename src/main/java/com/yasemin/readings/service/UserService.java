package com.yasemin.readings.service;

import com.yasemin.readings.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    void save(User user);

    User findByUsername(String username);

}