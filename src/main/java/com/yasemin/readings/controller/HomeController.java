package com.yasemin.readings.controller;

import com.yasemin.readings.service.QuoteService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequiredArgsConstructor
public class HomeController {

    private final QuoteService quoteService;

    @GetMapping(value = {"/", "/welcome"})
    public String greeting(Model model) {
        model.addAttribute("quote", quoteService.getRandomQuote());
        return "welcome";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

}
