package com.yasemin.readings.controller;

import com.yasemin.readings.service.QuoteService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.Principal;

@Controller
@RequiredArgsConstructor
public class QuoteController {

    private final QuoteService quoteService;

    @GetMapping(value = {"/quotes"})
    public String quotes(Model model, Principal principal) {
        model.addAttribute("quotes", quoteService.findAllExcept(principal.getName()));
        return "quotes";
    }

    @PostMapping(value = {"/addFavourite/{quoteId}"})
    public String addFavourite(@PathVariable Long quoteId, Principal principal) {
        quoteService.addToFavourites(principal.getName(), quoteId);
        return "redirect:/quotes";
    }

    @GetMapping(value = {"/favourites"})
    public String favouriteQuotes(Model model, Principal principal) {
        model.addAttribute("favouriteQuotes", quoteService.getFavourites(principal.getName()));
        return "favourite";
    }

    @PostMapping(value = {"/remove/{quoteId}"})
    public String removeFromFavourites(@PathVariable Long quoteId, Principal principal) {
        quoteService.removeFromFavourites(principal.getName(), quoteId);
        return "redirect:/favourites";
    }

}
